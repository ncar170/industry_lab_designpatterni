package ictgradschool.industry.lab_designpatterni.ex01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NestingShape extends Shape {

    private List<Shape> shapes;

    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        shapes = new ArrayList<>();
    }

    public void move(int width, int height) {



    }

    @Override
    public void paint(Painter painter) {

    }

    public void add(Shape child) throws IllegalArgumentException {

        Iterator<Shape> i = shapes.iterator();




    }

    public void remove (Shape child){

    }

    public Shape shapeAt (int index) throws IndexOutOfBoundsException {
        return null;
    }

    public int shapeCount (){
        return 0;
    }

    public int indexOf (Shape child){
        return 0;
    }

    public boolean contains (Shape child){
        return true;
    }
}
